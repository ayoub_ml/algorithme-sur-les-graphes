# include <assert.h> 
# include "chemin.h"

void tour_plus_court_chemin(){

	unsigned x,y,z ;
	for (y=0 ; y<nb_sommet ; y++){
		for (x = 0 ; x< nb_sommet ; x++ ){
			if (mat_adj[x][y]){
				for (z=0; z<nb_sommet; z++){
					if (mat_adj[y][z]>0){
						if((!mat_adj[x][y]) || (mat_adj[x][y] + mat_adj[y][z] < mat_adj[x][y] ))
						mat_adj[x][y] =  mat_adj[x][y] + mat_adj[y][z] ;
					}
				}
			}
		}
	}
}

unsigned moyen(unsigned x, unsigned y){
	return(mat_adj[x][y]!=0);
}

unsigned court (unsigned x , unsigned y){   /* ___________ complexité o(1) ___________ */
	assert(moyen(x,y));
	return(mat_adj[x][y]);
}